FROM python:3.12.4-alpine3.20 AS build

WORKDIR /app

RUN apk update && apk add poetry gcc python3-dev musl-dev linux-headers

COPY pyproject.toml LICENSE README.md poetry.lock src/ tests/ /app/

ENV TIKTOKEN_CACHE_DIR="/app/huggingface" \
    DATA_GYM_CACHE_DIR="/app/huggingface"

RUN poetry build && poetry install &&\
    source $(poetry env info --path)/bin/activate; echo -e 'import tiktoken\ntiktoken.encoding_for_model("gpt-3.5-turbo")' | python &&\
    find /app


FROM python:3.12.4-alpine3.20

WORKDIR /app

COPY --from=build /app/dist/*.whl /app/huggingface /app/

ENV TIKTOKEN_CACHE_DIR="/app/huggingface" \
    DATA_GYM_CACHE_DIR="/app/huggingface"

RUN pip3 install /app/*.whl && rm /app/*.whl

ENTRYPOINT [ "openaipy" ]
