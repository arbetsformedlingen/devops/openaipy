from os.path import isfile
import jsonpickle
import pytest
from openaipy import functions
from dotenv import load_dotenv
import os


settings = {
        'sysprompt': "You are a chat system with dyslexia, so your responses are spelled wrong.",
        'queryprompt': "Here is your assignment: ",
        'openaimodel': "gpt-3.5-turbo",
        'modelmaxresponse': 4096, # this is documented for each model on OpenAI
        'openaitemp': 0.99,
        'api_key': "",
        'desiredresponselength': 100,
        'sessionfile': os.getcwd() + "/chat.log", # optional
        'reiterate_sysprompt_interval': 2, # optional
}


def test_send_message():
        load_dotenv()
        initsettings = functions.init(os.getenv('OPENAI_API_KEY', ''))
        allsettings = initsettings | settings
        message_log = [
            {"role": "system", "content": settings['sysprompt']},
            {"role": "user", "content": allsettings['queryprompt'] + "What would you answer if you met your friends in the street?"}
        ]
        response = functions.send_message(allmessage_log)
        assert response and not response.isspace()


def test_converse():
        load_dotenv()
        initsettings = functions.init(os.getenv('OPENAI_API_KEY', ''))
        allsettings = initsettings | settings
        message_log = []
        if 'sessionfile' in allsettings and isfile(allsettings['sessionfile']):
            os.remove(allsettings['sessionfile'])
        response, message_log, allsettings = functions.converse(allmessage_log, "What would you answer if you met your friends in the street?")
        assert len(message_log) == 3
        assert message_log[-1]['role'] == "assistant"
        assert message_log[-1]['content'] == response
        assert response and not response.isspace()
        assert settings['sessionfile'] and isfile(settings['sessionfile']) and os.access(settings['sessionfile'],os.R_OK)
        with open(settings['sessionfile'], "r") as infile:
            data = infile.read()
            msglog = jsonpickle.decode(data)
        assert len(msglog) == 3
        response, message_log, allsettings = functions.converse(allmessage_log, "Could you suggest another reply?")
        assert len(message_log) == 5
        # here we hit the reiterate_sysprompt_interval limit, so a prompt is inserted too
        response, message_log, allsettings = functions.converse(allmessage_log, "Could you suggest yet another reply?")
        print(message_log)
        assert len(message_log) == 8


def test_huge_query():
    longstr = """This section is about the colour White. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Yellow. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Blue. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Red. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Green. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Black. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Brown. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Azure. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Ivory. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Teal. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Silver. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Purple. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Navy blue. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Pea green. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Gray. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Orange. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Maroon. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Charcoal. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Aquamarine. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Coral. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Fuchsia. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Wheat. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Lime. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Crimson. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Khaki. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Hot pink. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Magenta. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Olden. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Plum. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Olive. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
This section is about the colour Cyan. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY]."""
    load_dotenv()
    initsettings = functions.init(os.getenv('OPENAI_API_KEY', ''))
    allsettings = initsettings | settings
    allsettings['sysprompt'] = """You are a colour expert, with a special skill of replacing the string [SUMMARY] with a short summary about the colour being discussed.
        
        Here is an example of a good replacement.
         
        If you are presented with this sentence:
        This section is about the colour Olive. More text will be added here soon. The special qualities of this color can be summaried as [SUMMARY].
        
        You should replace [SUMMARY] with a summary about the colour being discussed:
        This section is about the colour Olive. More text will be added here soon. The special qualities of this color can be summaried as a darkish green color, the same colour as greek olives."""
    fill = "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255 256 257 258 259 260 261 262 263 264 265 266 267 268 269 270 271 272 273 274 275 276 277 278 279 280 281 282 283 284 285 286 287 288 289 290 291 292 293 294 295 296 297 298 299 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319 320 321 322 323 324 325 326 327 328 329 330 331 332 333 334 335 336 337 338 339 340 341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360 361 362 363 364 365 366 367 368 369 370 371 372 373 374 375 376 377 378 379 380 381 382 383 384 385 386 387 388 389 390 391 392 393 394 395 396 397 398 399 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 419 420 421 422 423 424 425 426 427 428 429 430 431 432 433 434 435 436 437 438 439 440 441 442 443 444 445 446 447 448 449 450 451 452 453 454 455 456 457 458 459 460 461 462 463 464 465 466 467 468 469 470 471 472 473 474"
    allsettings['queryprompt'] = "Please process the text you recieved about colors."
    message_log = [
        {"role": "system", "content": allsettings['sysprompt'] },
        {"role": "user", "content": allsettings['queryprompt'] + " " + longstr + longstr + longstr + fill}
    ]
    response = None
    try:
        response = functions.send_message(allmessage_log)
    except Exception as inst:
        print("exception: " + str(inst))
    assert response and not response.isspace()


# fixme: add test for two calls to bigsearch in sequence, to smoke out state
def test_bigsearch():
        load_dotenv()
        initsettings = functions.init(os.getenv('OPENAI_API_KEY', ''))
        allsettings = initsettings | settings
        allsettings['sysprompt'] = "Du är SSYKGPT, en språkmodell som analyserar en jobbannons och tilldelar den en yrkeskod i SSYK-standard från tabellen nedan. Ditt svar skall endast bestå av en fyrställig SSYK-kod, och du måste välja en kod som finns i denna tabell:"
        allsettings['queryprompt'] = "Här är är en jobbannons du ska analysera: "
        allsettings.pop('sessionfile', None)
        bigsearchdatafilterrx = r"^.*(\d\d\d\d).*$"
        bigsearchfile = "code+labels+desc.txt"
        message_log = []
        if 'sessionfile' in allsettings and allsettings['sessionfile'] and isfile(allsettings['sessionfile']):
            os.remove(allsettings['sessionfile'])
        response, message_log, allsettings = functions.bigsearch(allmessage_log, bigsearchfile, bigsearchdatafilterrx,
                                                                "Bagare sökes till ett kombinerat konditori och bageri. Erfarenhet av bakande och bröd krävs.")
        # fixme: add asserts


def test_manybigsearch():
        ads = [ '{"originalJobPosting": {"removed": null, "must_have": {"languages": null, "work_experiences": null, "skills": null}, "occupation_field": {"label": "Hälso- och sjukvård", "legacy_ams_taxonomy_id": "8", "concept_id": "NYW6_mP6_vwf"}, "scraper": "2006.100.jsonl", "employer": {"email": null, "phone_number": null, "url": null, "name": "Poolia Vård AB", "organization_number": null, "workplace": null}, "source_type": null, "org_description": {"company_information": null, "text": "Vi har behov for operasjonssykepleiere til langtidsoppdrag i Norge til høsten. Vi trenger deg fra uke 34, du må gjerne ha kompetanse på nevrokirurgi, brannsakder, hode/ hals.  Du har: minimum 2 års erfaring som sykepleier og Norsk legitimasjon", "requirements": null, "conditions": "100% stilling", "needs": null, "text_formatted": null}, "occupation_group": {"label": "Operationssjuksköterskor", "legacy_ams_taxonomy_id": "2231", "concept_id": "cuaN_Rj5_YCc"}, "driving_license_required": null, "salary_description": null, "publication_date": "2006-07-11", "other_old_legacy_attributes": {"TILLTRADE": null, "DXA": null, "BIL_KRAV": null, "ARBETSDRIFT": "Dagtid"}, "logo_url": null, "workplace_address": {"region_code": null, "municipality": null, "region_concept_id": null, "municipality_code": null, "postcode": null, "street_address": null, "country": "Norge", "country_concept_id": null, "city": null, "municipality_concept_id": null, "coordinates": null, "region": null, "country_code": null}, "application_deadline": "2006-08-30", "removed_date": null, "description": "Vi har behov for operasjonssykepleiere til langtidsoppdrag i Norge til høsten. Vi trenger deg fra uke 34, du må gjerne ha kompetanse på nevrokirurgi, brannsakder, hode/ hals.  Du har: minimum 2 års erfaring som sykepleier og Norsk legitimasjon", "application_details": {"via_af": null, "other": null, "information": null, "email": null, "url": null, "reference": null}, "last_publication_date": "2006-08-30", "number_of_vacancies": "50", "duration": {"concept_id": null, "legacy_ams_taxonomy_id": null, "label": "3-6 månader"}, "access_to_own_car": null, "employment_type": {"legacy_ams_taxonomy_id": null, "label": null, "concept_id": null}, "experience_required": null, "nice_to_have": {"languages": null, "skills": null, "work_experiences": null}, "scope_of_work": {"max": null, "min": null}, "id": null, "title": "OPERASJONSSYKEPLEIERE TIL NORGE", "timestamp": null, "webpage_url": null, "working_hours_type": {"legacy_ams_taxonomy_id": null, "label": "Heltid", "concept_id": null}, "driving_license": null, "external_id": null, "salary_type": {"label": null, "legacy_ams_taxonomy_id": null, "concept_id": null}, "headline": "OPERASJONSSYKEPLEIERE TIL NORGE", "occupation": {"legacy_ams_taxonomy_id": "5781", "label": "Operationssjuksköterska", "concept_id": "YJKc_yXU_DUH"}, "access": null}, "firstSeen": "2023-01-31T15:55:17", "id": "d41d8cd98f00b204e9800998ecf8427e", "detected_language": "no", "text_enrichments_results": {"enriched_result": {"enriched_candidates": {"occupations": [{"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false, "prediction": 0.605962932}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false, "prediction": 0.538536161}, {"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false, "prediction": 0.9953252971}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false, "prediction": 0.9994916916}], "competencies": [{"concept_label": "Förskola", "term": "förskola", "term_misspelled": false, "prediction": 0.602940917}, {"concept_label": "Förskola", "term": "förskolan", "term_misspelled": false, "prediction": 0.453326166}, {"concept_label": "Kompetensutveckling", "term": "kompetensutveckling", "term_misspelled": false, "prediction": 0.06992048}, {"concept_label": "Handledning", "term": "handledning", "term_misspelled": false, "prediction": 0.152150631}], "traits": [{"concept_label": "Pedagogisk", "term": "pedagogisk", "term_misspelled": false, "prediction": 0.0346379876}], "geos": [{"concept_label": "Täby", "term": "täby", "term_misspelled": false, "prediction": 0.968114674}, {"concept_label": "Näsby", "term": "näsby", "term_misspelled": false, "prediction": 0.926347256}]}}, "enrichedbinary_result": {"enriched_candidates": {"occupations": [{"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false}], "competencies": [{"concept_label": "Förskola", "term": "förskola", "term_misspelled": false}], "traits": [], "geos": [{"concept_label": "Näsby", "term": "näsby", "term_misspelled": false}, {"concept_label": "Täby", "term": "täby", "term_misspelled": false}]}}}}',
           # '{"firstSeen": "2023-01-31T15:55:17", "id": "d41d8cd98f00b204e9800998ecf8427e", "originalJobPosting": {"headline": "Jobba som värvare för Greenpeace!", "salary_type": {"concept_id": null, "legacy_ams_taxonomy_id": null, "label": null}, "occupation": {"label": "Torgförsäljare/Marknadsförsäljare", "legacy_ams_taxonomy_id": "7160", "concept_id": "MgLp_anW_jr5"}, "access": null, "external_id": null, "working_hours_type": {"concept_id": null, "label": "Deltid", "legacy_ams_taxonomy_id": null}, "driving_license": null, "id": null, "title": "Jobba som värvare för Greenpeace!", "webpage_url": null, "timestamp": null, "number_of_vacancies": "6", "last_publication_date": "2006-07-31", "experience_required": null, "duration": {"legacy_ams_taxonomy_id": null, "label": "11 dagar - 3 månader", "concept_id": null}, "access_to_own_car": null, "employment_type": {"concept_id": null, "legacy_ams_taxonomy_id": null, "label": null}, "nice_to_have": {"work_experiences": null, "skills": null, "languages": null}, "scope_of_work": {"min": null, "max": null}, "removed_date": null, "application_details": {"other": null, "via_af": null, "reference": null, "email": null, "information": null, "url": null}, "description": "Detta är det perfekta jobbet ! Arbetstiderna är 11-18, vardagar och du träffar många härliga människor!   I år har vi gått vi in på vårt åttonde år med Direkt Dialog som främsta metod att skaffa nya månadsgivare. Eftersom Greenpeace inte tar emot gåvor från företag, föreningar eller stater, är vi helt beroende av privatpersoner som vill stödja vårt viktiga miljöarbete.  Vi söker dig som inser vikten av en engagerad allmänhet och som tror på Greenpeace vision om en friskare planet.  Kort sagt - en person som tror att det går att förändra världen till det bättre! Som värvare talar du med folk på gator och torg i syfte att få dem att bidra ekonomiskt till Greenpeace arbete. För rätt person är det ett mycket givande arbete där kunskap och trevliga arbetskamrater blir en central del även om jobbet i sig är tufft.   För att passa till jobbet ska du tycka om att arbeta mot konkreta mål, vara utåtriktad och självgående samt ha en hög arbetsmoral. Dessa egenskaper är viktiga eftersom arbetet innebär frihet under ansvar. Givetvis får du utbildning och löpande coachning av Greenpeace. Du måste också ha goda kunskaper i svenska språket.   Vi ser fram emot din ansökan.  Tycker du att jobbet låter spännande? I så fall så skickar du din ansökan till Lena-Olivia Andersson.", "workplace_address": {"country_code": null, "region": null, "coordinates": null, "city": "STOCKHOLM", "municipality_concept_id": null, "postcode": null, "street_address": null, "municipality_code": "2480", "country_concept_id": null, "country": null, "region_concept_id": null, "region_code": null, "municipality": null}, "application_deadline": "2006-07-31", "salary_description": null, "driving_license_required": null, "occupation_group": {"legacy_ams_taxonomy_id": "9520", "label": "Torg- och marknadsförsäljare m.fl.", "concept_id": "xyW2_toA_Skh"}, "other_old_legacy_attributes": {"ARBETSDRIFT": "Dagtid", "DXA": null, "TILLTRADE": null, "BIL_KRAV": null}, "publication_date": "2006-07-18", "logo_url": null, "removed": null, "must_have": {"skills": null, "work_experiences": null, "languages": null}, "occupation_field": {"legacy_ams_taxonomy_id": "5", "label": "Försäljning, inköp, marknadsföring", "concept_id": "RPTn_bxG_ExZ"}, "employer": {"name": "Greenpeace Norden", "url": null, "workplace": null, "organization_number": null, "email": null, "phone_number": null}, "scraper": "2006.100.jsonl", "source_type": null, "org_description": {"requirements": null, "text": "Detta är det perfekta jobbet ! Arbetstiderna är 11-18, vardagar och du träffar många härliga människor!   I år har vi gått vi in på vårt åttonde år med Direkt Dialog som främsta metod att skaffa nya månadsgivare. Eftersom Greenpeace inte tar emot gåvor från företag, föreningar eller stater, är vi helt beroende av privatpersoner som vill stödja vårt viktiga miljöarbete.  Vi söker dig som inser vikten av en engagerad allmänhet och som tror på Greenpeace vision om en friskare planet.  Kort sagt - en person som tror att det går att förändra världen till det bättre! Som värvare talar du med folk på gator och torg i syfte att få dem att bidra ekonomiskt till Greenpeace arbete. För rätt person är det ett mycket givande arbete där kunskap och trevliga arbetskamrater blir en central del även om jobbet i sig är tufft.   För att passa till jobbet ska du tycka om att arbeta mot konkreta mål, vara utåtriktad och självgående samt ha en hög arbetsmoral. Dessa egenskaper är viktiga eftersom arbetet innebär frihet under ansvar. Givetvis får du utbildning och löpande coachning av Greenpeace. Du måste också ha goda kunskaper i svenska språket.   Vi ser fram emot din ansökan.  Tycker du att jobbet låter spännande? I så fall så skickar du din ansökan till Lena-Olivia Andersson.", "needs": null, "conditions": "6 timmar om dagen mellan 11 och 18, mån-fre varaktighet 1-3 mån", "text_formatted": null, "company_information": null}}, "detected_language": "sv", "text_enrichments_results": {"enriched_result": {"enriched_candidates": {"occupations": [{"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false, "prediction": 0.605962932}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false, "prediction": 0.538536161}, {"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false, "prediction": 0.9953252971}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false, "prediction": 0.9994916916}], "competencies": [{"concept_label": "Förskola", "term": "förskola", "term_misspelled": false, "prediction": 0.602940917}, {"concept_label": "Förskola", "term": "förskolan", "term_misspelled": false, "prediction": 0.453326166}, {"concept_label": "Kompetensutveckling", "term": "kompetensutveckling", "term_misspelled": false, "prediction": 0.06992048}, {"concept_label": "Handledning", "term": "handledning", "term_misspelled": false, "prediction": 0.152150631}], "traits": [{"concept_label": "Pedagogisk", "term": "pedagogisk", "term_misspelled": false, "prediction": 0.0346379876}], "geos": [{"concept_label": "Täby", "term": "täby", "term_misspelled": false, "prediction": 0.968114674}, {"concept_label": "Näsby", "term": "näsby", "term_misspelled": false, "prediction": 0.926347256}]}}, "enrichedbinary_result": {"enriched_candidates": {"occupations": [{"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false}], "competencies": [{"concept_label": "Förskola", "term": "förskola", "term_misspelled": false}], "traits": [], "geos": [{"concept_label": "Näsby", "term": "näsby", "term_misspelled": false}, {"concept_label": "Täby", "term": "täby", "term_misspelled": false}]}}}}',
           # '{"id": "d41d8cd98f00b204e9800998ecf8427e", "firstSeen": "2023-01-31T15:55:17", "originalJobPosting": {"webpage_url": null, "timestamp": null, "id": null, "title": "Au Pair i Paris hos familjen Picquard", "scope_of_work": {"min": null, "max": null}, "nice_to_have": {"languages": null, "skills": null, "work_experiences": null}, "experience_required": null, "access_to_own_car": null, "employment_type": {"concept_id": null, "legacy_ams_taxonomy_id": null, "label": null}, "duration": {"concept_id": null, "label": "Längre än 6 månader", "legacy_ams_taxonomy_id": null}, "number_of_vacancies": "1", "last_publication_date": "2006-08-30", "access": null, "salary_type": {"concept_id": null, "label": null, "legacy_ams_taxonomy_id": null}, "headline": "Au Pair i Paris hos familjen Picquard", "occupation": {"label": null, "legacy_ams_taxonomy_id": "5552", "concept_id": null}, "external_id": null, "driving_license": null, "working_hours_type": {"concept_id": null, "legacy_ams_taxonomy_id": null, "label": "Deltid"}, "logo_url": null, "other_old_legacy_attributes": {"BIL_KRAV": null, "TILLTRADE": null, "DXA": null, "ARBETSDRIFT": "Dagtid"}, "publication_date": "2006-05-05", "salary_description": null, "occupation_group": {"label": null, "legacy_ams_taxonomy_id": null, "concept_id": null}, "driving_license_required": null, "org_description": {"text": "Familj i Paris med två barn på 6 och 4 år söker dig som älskar barn och vill bli del utav en franska familj under ett år. Familjen bor i hus där du har eget rum och eget badrum. Båda barnen går i skolan, den yngsta endast förmiddagar. Mamman arbetar som Admin chef på ett konferens företag. Pappan är läkare. Ungdomlig och social familj som gärna tar dig med på sina utflykter osv på helgerna. De har haft Au Pairer genom agenturen tidigare och alla har trivts mycket bra.  Underbart läge, nära till allt; affärer, restauranger, skolor, metro, andra Au Pairer osv.   Familjens största intressen är golf, fotboll, tennis, segelflygning, resa, cykla, naturen, resor, musik, matlagning.  Familjen behöver din hjälp med att ta hand om barnen när föräldrarna arbetar. Ditt arbete kommer innebära att förbereda frukost och hjälpa till på mornarna så att de kommer iväg till skolan, följa dem, hämta dem, hitta på aktiviteter med barnen när ni är ensamna, ta en promenad till parken, läsa, gå  på bio eller museum, följa dem till eftermiddagsaktiviteter så som balettlektion och simskola, hjälpa till med läxläsning, leka utomhus, mm.   Familjen söker dig som har varit barnvakt eller tagit hand om barn tidigare och som älskar barn. De behöver någon som är positiv och flexibel och som tycker om när det händer saker. Ett plus om du har läst franska iallafall i ett par år. Du måste vara rökfri.", "requirements": null, "conditions": "Startdatum September för 9-12 månader. 25hr/ vecka. Helger lediga, barnvakt 1-2kv/vecka", "needs": null, "text_formatted": null, "company_information": null}, "source_type": null, "scraper": "2006.100.jsonl", "employer": {"organization_number": null, "workplace": null, "url": null, "name": "Work & Study Abroad Stockholm", "phone_number": null, "email": null}, "occupation_field": {"legacy_ams_taxonomy_id": null, "label": null, "concept_id": null}, "must_have": {"skills": null, "work_experiences": null, "languages": null}, "removed": null, "application_details": {"url": null, "information": null, "email": null, "reference": null, "via_af": null, "other": null}, "description": "Familj i Paris med två barn på 6 och 4 år söker dig som älskar barn och vill bli del utav en franska familj under ett år. Familjen bor i hus där du har eget rum och eget badrum. Båda barnen går i skolan, den yngsta endast förmiddagar. Mamman arbetar som Admin chef på ett konferens företag. Pappan är läkare. Ungdomlig och social familj som gärna tar dig med på sina utflykter osv på helgerna. De har haft Au Pairer genom agenturen tidigare och alla har trivts mycket bra.  Underbart läge, nära till allt; affärer, restauranger, skolor, metro, andra Au Pairer osv.   Familjens största intressen är golf, fotboll, tennis, segelflygning, resa, cykla, naturen, resor, musik, matlagning.  Familjen behöver din hjälp med att ta hand om barnen när föräldrarna arbetar. Ditt arbete kommer innebära att förbereda frukost och hjälpa till på mornarna så att de kommer iväg till skolan, följa dem, hämta dem, hitta på aktiviteter med barnen när ni är ensamna, ta en promenad till parken, läsa, gå  på bio eller museum, följa dem till eftermiddagsaktiviteter så som balettlektion och simskola, hjälpa till med läxläsning, leka utomhus, mm.   Familjen söker dig som har varit barnvakt eller tagit hand om barn tidigare och som älskar barn. De behöver någon som är positiv och flexibel och som tycker om när det händer saker. Ett plus om du har läst franska iallafall i ett par år. Du måste vara rökfri.", "removed_date": null, "application_deadline": "2006-08-30", "workplace_address": {"postcode": null, "street_address": null, "municipality_code": null, "country_concept_id": null, "country": null, "city": "STOCKHOLM", "municipality_concept_id": null, "region_code": null, "municipality": null, "region_concept_id": null, "region": null, "country_code": null, "coordinates": null}}, "detected_language": "sv", "text_enrichments_results": {"enriched_result": {"enriched_candidates": {"occupations": [{"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false, "prediction": 0.605962932}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false, "prediction": 0.538536161}, {"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false, "prediction": 0.9953252971}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false, "prediction": 0.9994916916}], "competencies": [{"concept_label": "Förskola", "term": "förskola", "term_misspelled": false, "prediction": 0.602940917}, {"concept_label": "Förskola", "term": "förskolan", "term_misspelled": false, "prediction": 0.453326166}, {"concept_label": "Kompetensutveckling", "term": "kompetensutveckling", "term_misspelled": false, "prediction": 0.06992048}, {"concept_label": "Handledning", "term": "handledning", "term_misspelled": false, "prediction": 0.152150631}], "traits": [{"concept_label": "Pedagogisk", "term": "pedagogisk", "term_misspelled": false, "prediction": 0.0346379876}], "geos": [{"concept_label": "Täby", "term": "täby", "term_misspelled": false, "prediction": 0.968114674}, {"concept_label": "Näsby", "term": "näsby", "term_misspelled": false, "prediction": 0.926347256}]}}, "enrichedbinary_result": {"enriched_candidates": {"occupations": [{"concept_label": "Förskollärare", "term": "förskollärare", "term_misspelled": false}, {"concept_label": "Barnskötare", "term": "barnskötare", "term_misspelled": false}], "competencies": [{"concept_label": "Förskola", "term": "förskola", "term_misspelled": false}], "traits": [], "geos": [{"concept_label": "Näsby", "term": "näsby", "term_misspelled": false}, {"concept_label": "Täby", "term": "täby", "term_misspelled": false}]}}}}'  ]
                ]
        load_dotenv()
        initsettings = functions.init(os.getenv('OPENAI_API_KEY', ''))
        allsettings = initsettings | settings
        allsettings['sysprompt'] = "Du är SSYKGPT, en språkmodell som analyserar en jobbannons och tilldelar den en yrkeskod i SSYK-standard från tabellen nedan. Ditt svar skall endast bestå av en fyrställig SSYK-kod, och du måste välja en kod som finns i denna tabell:"
        allsettings['queryprompt'] = "Här är är en jobbannons du ska analysera: "
        allsettings.pop('sessionfile', None)
        bigsearchdatafilterrx = r"^.*(\d\d\d\d).*$"
        bigsearchfile = "code+labels+desc.txt"
        if 'sessionfile' in allsettings and allsettings['sessionfile'] and isfile(allsettings['sessionfile']):
            os.remove(allsettings['sessionfile'])
        for ad in ads:
            response, message_log, allsettings = functions.bigsearch(allsettings, bigsearchfile, bigsearchdatafilterrx, ad)
            print(response)

def test_remove_old_sysprompts():
    # Test that the function removes all old sysprompts except for the last one:
    message_log = [
        {"role": "user", "message": "Hello!"},
        {"role": "sysprompt", "message": "Please enter your name"},
        {"role": "user", "message": "John"},
        {"role": "sysprompt", "message": "How can I assist you?"}
    ]

    expected_output = [
        {"role": "user", "message": "Hello!"},
        {"role": "user", "message": "John"},
        {"role": "sysprompt", "message": "How can I assist you?"}
    ]

    assert functions.remove_old_sysprompts(message_log) == expected_output

    # Test that the function returns the same message log if there are no sysprompts:
    message_log = [
        {"role": "user", "message": "Hello!"},
        {"role": "user", "message": "How can I help you?"}
    ]

    assert functions.remove_old_sysprompts(message_log) == message_log

    # Test that the function correctly handles an empty message log:
    message_log = []

    assert functions.remove_old_sysprompts(message_log) == message_log

    # Test that the function raises an exception if the input is not a list:
    message_log = "hello"

    try:
        functions.remove_old_sysprompts(message_log)
    except TypeError:
        pass
    else:
        raise AssertionError("Expected TypeError")
