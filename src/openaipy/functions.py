import re
import logging
import re
import time
import traceback
from os import access, R_OK, W_OK
from os.path import isfile

import jsonpickle
import openai
import tiktoken


# ny funktion:
# - preprocess
#   - regex -> google -> add text -> openai
# - postprocess
#   - replace regex patterns
#   - censor (just calls replace with '')
#   - enrichment: regex -> ask gpt for elaboration -> join answers -> return to user
#   -             regex -> ask for curl -> run curl -> gpt -> return to user



def init(api_key):
    openai.api_key = api_key
    FORMAT = '%(asctime)s %(message)s'
    logging.basicConfig(format=FORMAT)
    _logger = logging.getLogger(__name__)
    #_logger.setLevel(logging.DEBUG)
    return {
        'max_retries': 3,
        'query_timeout': 10,
        'logger': _logger,
    }




def bigsearch(settings, message_log, bigsearchfile, bigsearchdatafilterrx, text):
    if bigsearchfile and isfile(bigsearchfile) and access(bigsearchfile, R_OK):
        with open(bigsearchfile, "r") as infile:
            bigdata = infile.read()
            settings['logger'].info("read " + bigsearchfile)
    else:
        raise Exception("file not found: " + bigsearchfile)
    maxtoks = settings['modelmaxresponse']
    margin = 400
    texttoks = num_tokens_from_string(text, settings['openaimodel'])
    bigdatachunksize = int(maxtoks - margin - texttoks - settings['desiredresponselength']) - 1
    settings['logger'].info("bigsearch chunksize: " + str(bigdatachunksize))

    bigsearchdatafilter = lambda response, data: [e for e in data if re.sub(bigsearchdatafilterrx, r"\1", response) in e]

    orgsysprompt = settings['sysprompt']
    bigdatalines = bigdata.splitlines()
    filtered_data_list = []

    while bigdatalines:
        curchunktoks = 0
        curchunk = []
        line = ""
        while bigdatalines and curchunktoks < bigdatachunksize:
            line = bigdatalines.pop(0)
            curchunktoks += num_tokens_from_string(line, settings['openaimodel'])
            curchunk.append(line)
        if curchunktoks >= bigdatachunksize:
            curchunktoks -= num_tokens_from_string(line, settings['openaimodel'])
            curchunk.pop(-1)
            bigdatalines.insert(0, line)

        settings['sysprompt'] = orgsysprompt + "\n\n" + "\n".join(curchunk)
        settings['logger'].debug("bigsearching "+ str(len(curchunk)) +" lines with sysprompt: " + settings['sysprompt'])
        response, _, _ = converse(settings, [], text)
        settings['logger'].debug("response for sysprompt: " + response + " (lines left to search: " + str(len(bigdatalines)) + ")")
        filtered_data = bigsearchdatafilter(response, curchunk)

        if not filtered_data:
            settings['logger'].warn("no match in data chunk for response " + response + ". Datachunk: " + "\n".join(curchunk))
        else:
            filtered_data_str = "\n".join(filtered_data)
            settings['logger'].info("match in data chunk for response: " + filtered_data_str)
            filtered_data_list.append(filtered_data_str)

    settings['sysprompt'] = orgsysprompt
    if len(filtered_data_list) == 0:
        settings['logger'].warn("No results for prompt: " + text)
        return "notfound", message_log, settings
    elif len(filtered_data_list) == 1:
        settings['logger'].info("Exactly one result for prompt: " + text)
        return filtered_data_list.pop(0), message_log, settings

    filtered_data = "\n\n".join(filtered_data_list)
    settings['sysprompt'] = orgsysprompt + "\n\n" + filtered_data
    settings['logger'].debug("sysprompt for final select call: " + settings['sysprompt'])
    response, message_log, settings = converse(settings, message_log, text)
    settings['sysprompt'] = orgsysprompt
    return response, message_log, settings


def remove_old_sysprompts(message_log):
    idx_sysprompts = [idx for idx, m in enumerate(message_log) if m["role"] == "sysprompt"]
    if idx_sysprompts:
        idx_sysprompts.pop()
    for i in idx_sysprompts[::-1]:
        message_log.pop(i)
    return message_log


def truncate(settings, message_log):
    message_log = remove_old_sysprompts(message_log)
    length = 0
    while message_log:
        try:
            length = compute_completion_length(settings,message_log)
        except Exception:
            length = -1
            message_log.pop(0)
        if length >= 0:
            break
    message_log = message_log[-6:] # keep 6 last element of discussion FIXME: make a config param
    return message_log


def converse(settings, message_log, text):
    if 'sessionfile' in settings and settings['sessionfile'] and isfile(settings['sessionfile']) and access(settings['sessionfile'], R_OK):
        with open(settings['sessionfile'], "r") as infile:
            data = infile.read()
            message_log = jsonpickle.decode(data)
            settings['logger'].info("read " + settings['sessionfile'] +" with " + str(len(message_log)) + ' rows')
    if 'reiterate_sysprompt_interval' in settings and settings['reiterate_sysprompt_interval'] and 'prompt_reiter_count' not in settings:
        settings['prompt_reiter_count'] = settings['reiterate_sysprompt_interval']
    if not message_log or ('prompt_reiter_count' in settings and 'reiterate_sysprompt_interval' in settings and settings['reiterate_sysprompt_interval'] and settings['prompt_reiter_count'] >= settings['reiterate_sysprompt_interval']):
        message_log.append({"role": "system", "content": settings['sysprompt'] })
        settings['prompt_reiter_count'] = 0
    if 'prompt_reiter_count' in settings:
        settings['prompt_reiter_count'] += 1
    size_bef = len(message_log)
    message_log = message2chatlog(message_log, settings, "user", text)
    message_log[size_bef]['content'] = settings['queryprompt'] + "\n\n" + 'Text: """' + "\n" + message_log[size_bef]['content']
    message_log[-1]['content'] += "\n" + '"""'
    message_log = truncate(settings, message_log)
    response = send_message(settings, message_log)
    message_log.append({"role": "assistant", "content": response})
    if 'sessionfile' in settings and settings['sessionfile'] and ( ( not isfile(settings['sessionfile']) ) or access(settings['sessionfile'], W_OK) ):
        with open(settings['sessionfile'], "w") as outfile:
            data = jsonpickle.encode(message_log)
            outfile.write(data)
            settings['logger'].info("wrote " + settings['sessionfile']+" with "+str(len(message_log))+" rows")
    log_message_log(settings, message_log, "Message log at end of converse().")
    return response, message_log, settings


def log_message_log(settings, message_log, header):
    settings['logger'].info(header + "   Message_log tokens: " + str(calc_msglog_len(message_log, settings['openaimodel'])))
    for m in message_log:
        #settings['logger'].info("\t" + m['role'] + "\t" + m['content'])
        settings['logger'].info("\t" + m['role'] + "\t" + ' '.join(((m['content'][:60] + ' ... ' + m['content'][-10:]) if len(m['content']) > 75 else m['content']).split()))

def message2chatlog(msglog, settings, role, msg):
    parts = splitonspace(msg, settings['modelmaxresponse'] - settings['desiredresponselength'] - 20, settings['openaimodel'])
    for p in parts:
        msglog.append({"role": role, "content": p})
    return msglog

def splitonspace(msg, size, model):
    result = []
    encoding = tiktoken.encoding_for_model(model)
    words = encoding.encode(msg)

    if len(words) < size:
        return [msg]


    current_length = 0
    current_part = ""

    for word in words:
        if len(word) > size:
            raise ValueError("Word too long to fit in the specified size.")
        if current_length + len(word) <= size:
            current_part += word + " "
            current_length += len(word) + 1
        else:
            result.append(current_part[:-1])
            current_part = word + " "
            current_length = len(word) + 1

    result.append(current_part[:-1])
    return result



def compute_completion_length(settings, message_log):
    maxtoks = settings['modelmaxresponse']
    weird_margin = 10 # for some reason, OpenAI adds some extra tokens to the requests
    per_msg = num_tokens_from_string("role", settings['openaimodel']) + num_tokens_from_string("content", settings['openaimodel']) + weird_margin
    for msg in message_log:
        maxtoks -= num_tokens_from_string(msg['content'], settings['openaimodel'])
        maxtoks -= num_tokens_from_string(msg['role'], settings['openaimodel'])
        maxtoks -= per_msg
    if maxtoks <= 0:
        settings['logger'].error('Too long prompt '+str(maxtoks)+', no room left for response in context')
        raise Exception('too long prompt '+str(maxtoks)+', no room left for response in context')
    if settings['desiredresponselength'] < maxtoks:
        maxtoks = settings['desiredresponselength']
    return maxtoks



def send_message(settings, message_log):
    maxtoks = compute_completion_length(settings, message_log)
    settings['logger'].info("Message has around " + str(maxtoks) + " tokens.")
    response = None
    sleeptime = 60
    for _ in range(settings['max_retries']):
        try:
            #settings['logger'].debug("sending request: "+str(message_log))
            response = openai.ChatCompletion.create(
                model=settings['openaimodel'],
                messages=message_log,
                max_tokens=maxtoks,
                stop=None,
                temperature=settings['openaitemp'],
                timeout=settings['query_timeout'],
            )
            break
        except openai.error.APIError as e:
            # Handle API error here, e.g. retry or log
            settings['logger'].warning(f"OpenAI API returned an API Error: {e}")
            pass
        except openai.error.APIConnectionError as e:
            # Handle connection error here
            settings['logger'].warning(f"Failed to connect to OpenAI API: {e}")
            pass
        except openai.error.RateLimitError as e:
            settings['logger'].warning(f"OpenAI API request exceeded rate limit: {e}")
            pass
        except TimeoutError:
            settings['logger'].warning('Query timed out...')
            pass
        except Exception as e:
            settings['logger'].warning(traceback.format_exc())
            pass
        settings['logger'].warning("Sleeping before retry: " + str(sleeptime))
        time.sleep(sleeptime)
        sleeptime *= 2

    if not response:
        return "[no-answer-from-api]"

    for choice in response.choices:
        if "text" in choice:
            #settings['logger'].info("got response (text): " + choice.text)
            return choice.text
    #settings['logger'].info("got response: "+response.choices[0].message.content)
    return response.choices[0].message.content


def num_tokens_from_string(string: str, model: str) -> int:
    """Returns the number of tokens in a text string."""
    encoding = tiktoken.encoding_for_model(model)
    num_tokens = len(encoding.encode(string))
    return num_tokens

def calc_msglog_len(msglog, model):
    leng = 0
    encoding = tiktoken.encoding_for_model(model)
    role_len = len(encoding.encode("role:"))
    content_len = len(encoding.encode("content:"))
    for m in msglog:
        leng += len(encoding.encode(m['role'])) + len(encoding.encode(m['content'])) + role_len + content_len
    return leng

