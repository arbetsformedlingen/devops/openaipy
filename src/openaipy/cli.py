from os.path import isfile
import click
from dotenv import load_dotenv
import os
from openaipy import functions



@click.argument("prompt", required=True)

@click.option("--sysprompt", "-s", type=str, required=False, help="")
@click.option("--syspromptfile", "-S", type=str, required=False, help="Overrides --sysprompt")
@click.option("--bigsearchfile", "-b", type=str, required=False, help="When searching for data in a set larger than modelmax, use this")
@click.option("--bigsearchdatafilterrx", "-Q", type=str, required=False, help="")
@click.option("--queryprompt", "-q", type=str, required=False, help="Use to prefix queries")
@click.option("--openaimodel", "-m", type=str, required=False, help="")
@click.option("--modelmaxresponse", "-r", type=int, required=False, help="")
@click.option("--openaitemp", "-t", type=float, required=False, help="")
@click.option("--api_key", "-k", type=str, required=False, help="")
@click.option("--desiredresponselength", "-l", type=int, required=False, help="")
@click.option("--sessionfile", "-f", type=str, required=False, help="")
@click.option("--reiterate_sysprompt_interval", "-R", type=int, required=False, help="")
@click.command(context_settings=dict(help_option_names=["-h", "--help"])) # alias -h to --help
def cli(sysprompt, syspromptfile, bigsearchfile, bigsearchdatafilterrx, queryprompt, prompt, openaimodel, modelmaxresponse, openaitemp, api_key, desiredresponselength, sessionfile, reiterate_sysprompt_interval) -> None:
    load_dotenv()
    settings = {
        'sysprompt': sysprompt or "You are a chat system with dyslexia, so your responses are spelled wrong.",
        'queryprompt': queryprompt or "",
        'bigsearchfile': bigsearchfile or "",
        'bigsearchdatafilterrx': bigsearchdatafilterrx or "",
        'openaimodel': openaimodel or "gpt-3.5-turbo",
        'modelmaxresponse': modelmaxresponse or 4096,  # this is documented for each model on OpenAI
        'openaitemp': openaitemp or 0.99,
        'api_key': api_key or os.getenv('OPENAI_API_KEY', ''),
        'desiredresponselength': desiredresponselength or 100,
    }
    if syspromptfile and isfile(syspromptfile) and os.access(syspromptfile, os.R_OK):
        with open(syspromptfile, "r") as infile:
            settings['sysprompt'] = infile.read()
    if sessionfile:
        settings['settingsfile'] = sessionfile
    if reiterate_sysprompt_interval:
        settings['reiterate_sysprompt_interval'] = reiterate_sysprompt_interval
    settings = settings | functions.init(settings['api_key'])
    if 'bigsearchfile' in settings and 'bigsearchdatafilterrx' in settings:
        response, message_log, settings = functions.bigsearch(settings, [], bigsearchfile, bigsearchdatafilterrx, prompt)
    else:
        response, message_log, settings = functions.converse(settings, [], prompt)
    print(response)


if __name__ == "__main__":
    cli()
