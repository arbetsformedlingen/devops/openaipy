# -*- mode: Makefile;-*-

.ONESHELL:

SHELL         := /bin/bash
.DEFAULT_GOAL := help



MAINSCRIPT ?= $(shell ls -1tar src/* | tail -n 1)



#:##
#:## #### DATA GENERATION
#:##


.PHONY: readme
readme: ## Extract a README.md from the Pod documentation in the main script.
	pod2markdown < $(MAINSCRIPT) | grep -q '\\`' \
	  && echo "warning: formatting errors detected" >&2
	pod2markdown < $(MAINSCRIPT) | sed 's|\\`|`|g'  > README.md



.PHONY: docs
docs: ## Generate documentation files in various formats in docs/
	mkdir -p docs
	pod2man < $(MAINSCRIPT) > docs/$$(basename $(MAINSCRIPT)).1
	pod2text < $(MAINSCRIPT) > docs/$$(basename $(MAINSCRIPT)).txt
	pod2html < $(MAINSCRIPT) > docs/$$(basename $(MAINSCRIPT)).html


#:##
#:##
#:## #### VERSION MANAGEMENT
#:##



.PHONY: bump-patch
bump-patch: ## Bump the patch version of this repo.
	@. <(curl -s https://gitlab.com/perweij/projtool/-/raw/master/releasetool.sh) || exit 1
	newver="$$(bump_patch)"
	add_changelogdata_from_commits gitlab_priv_access_token_notes "$$newver" "$$(get_proj_id)" || exit 1
	# pull the newly updated changelog from remote - possibly we should warn the user first here
	git pull $$(get_repo_remote_label) || exit 1
	git tag "v$$newver" || exit 1
	git push --tags || exit 1



.PHONY: changelog
changelog: ## generate changelog
	@. <(curl -s https://gitlab.com/perweij/projtool/-/raw/master/releasetool.sh)
	gen_changelogdata gitlab_priv_access_token_notes $(get_proj_id)



.PHONY: hard-set-version
hard-set-version: ## Set a hard version in this repo. Supply version using envvar VER (make hard-set-version VER=0.1.0)
	test -z "$(VER)" && { echo "error: set version with VER" >&2; exit 1; }
	git tag v$(VER) || exit 1
	git push --tags


#:##
#:##
#:## #### PROJECT TOOLS
#:##
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))


.PHONY: update-projtool
update-projtool: ## Update the makefile containing these tools
	curl -s -o "$(mkfile_path)" \
		-L "https://gitlab.com/perweij/projtool/-/raw/master/proj-templates/COMMON/.projtools"


chk-bef-init:
	@if [ ! -d .git ]; then
		echo "**** Directory not git controlled. Press Enter or CTRL-C" >&2
		read slask
	fi
	if [ -d .git ] && [ ! -z "$$(git status --porcelain)" ]; then \
		echo "**** Repo is not clean. Press Enter or CTRL-C" >&2
		read slask
	fi


init-bash: chk-bef-init
	@TMPD=$$(mktemp -d) || exit 1
	(
		curl -s -L "https://gitlab.com/perweij/projtool/-/archive/main/projtool-main.tar.bz2" \
		| tar -C "$$TMPD" -xvjf - || exit 1
		projdir=$$(pwd)
		projname="$$(basename $$projdir)"
		cd "$$TMPD"/projtool-main/proj-templates/COMMON || exit 1
		find . -mindepth 1 -type d -exec bash -c "mkdir -p '$$projdir/{}'" \; || exit 1
		find . -mindepth 1 -exec bash -c "test -f $$projdir/{} && diff $$projdir/{} {} || { echo ; cp --interactive {} $$projdir/{}; echo; }" \; || exit 1
		cd "$$TMPD"/projtool-main/proj-templates/bash || exit 1
		find . -mindepth 1 -type d -exec bash -c "mkdir -p '$$projdir/{}'" \; || exit 1
		find . -mindepth 1 -type f -exec bash -c "test -f $$projdir/{} && diff $$projdir/{} {} || { echo ; cp --interactive {} $$projdir/{}; echo; }" \; || exit 1
		cd "$$projdir"
		find . -type f -exec sed -i -E "s/\{\{NAME\}\}/$$projname/g" {} \;
		find . -type f -exec sed -i -E "s/(\{\{AUTHOR\}\}|Your Name)/$$(git config user.name)/g" {} \;
		find . -type f -exec sed -i -E "s/(\{\{EMAIL\}\}|you\@example.com)/$$(git config user.email)/g" {} \;
	)
	rm -rf "$$TMPD"


# todo: add https://pypi.org/project/poetry-lock-package/
init-pylib: chk-bef-init
	@TMPD=$$(mktemp -d) || exit 1
	(
		set -x
		projdir="$$(pwd)"
		projname="$$(basename $$projdir)"
		cd "$$projdir"
		test -f pyproject.toml || poetry new --name "$$projname" --src . || exit 1
		cat << EOF | tr '§' ' ' > tests/test_"$$projname".py
	from $$projname import __version__


	def test_version():
	§§§§assert __version__ == '0.1.0'
	EOF
		echo "__version__ = '0.1.0'" > src/"$$projname"/__init__.py
		poetry add --dev pytest
		rm -f README.rst README.md
		sed -i 's|pytest = "^..."|pytest = "^7.2"|' pyproject.toml
		curl -s -L "https://gitlab.com/perweij/projtool/-/archive/main/projtool-main.tar.bz2" \
		| tar -C "$$TMPD" -xjf - || exit 1
		#cp -a /home/weipe/proj/projtool "$$TMPD"/projtool-main #fixme
		cd "$$TMPD"/projtool-main/proj-templates/COMMON || exit 1
		find . -mindepth 1 -type d -exec bash -c "mkdir -p '$$projdir/{}'" \; || exit 1
		find . -mindepth 1 -exec bash -c "test -f $$projdir/{} && diff $$projdir/{} {} || { echo ; cp --interactive {} $$projdir/{}; echo; }" \; || exit 1
		cd "$$TMPD"/projtool-main/proj-templates/pylib || exit 1
		find . -mindepth 1 -type d -exec bash -c "mkdir -p '$$projdir/{}'" \; || exit 1
		find . -mindepth 1 -maxdepth 1 -type f -exec bash -c "test -f $$projdir/{} && diff $$projdir/{} {} || { echo ; cp --interactive {} $$projdir/{}; echo; }" \; || exit 1
		cd "$$projdir"
		test -f src/"$$projname"/functions.py || cp "$$TMPD"/projtool-main/proj-templates/pylib/src/functions.py src/"$$projname"/functions.py
		find . -type f -exec sed -i -E "s/\{\{NAME\}\}/$$projname/g" {} \;
		find . -type f -exec sed -i -E "s/(\{\{AUTHOR\}\}\|Your Name)/$$(git config user.name)/g" {} \;
		find . -type f -exec sed -i -E 's/(\{\{EMAIL\}\}|you\@example.com)/email\@example.com/g' {} \;
	)
	rm -rf "$$TMPD"



#### INTERNAL TARGETS
.PHONY: check-git check-tools
check-git:
	@if [ ! -z "$$(git status --porcelain)" ]; then \
		echo "**** error: repo is not clean" ;\
		exit 1 ;\
	fi
#	@if [ "$(BRANCH)" != "master" ]; then \
#		echo "**** error: not on master ($(BRANCH))" ;\
#		exit 1 ;\
#	fi
	git pull



.PHONY: check-tools
check-tools:
	for cmd in poetry; do
		if ! command -v $cmd >/dev/null; then
			echo "error: $cmd missing" >&2
			exit 1
		fi
	done


#:##
#:##
#:## #### USEFUL INFO
#:##

.PHONY: help
help:
	@grep --no-filename -E '^[\#a-zA-Z_-]+:.*?##' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "%-30s %s\n", $$1, $$2}' | perl -pe 's/^(.*?)(?=[^#:]|$$)/ " " x length($$1) /e' | perl -pe 's|^\s+####|####|'
	@echo "Gitlab changelog trailers:"
	@echo "  added:       New feature"
	@echo "  fixed:       Bug fix"
	@echo "  changed:     Feature change"
	@echo "  deprecated:  New deprecation"
	@echo "  removed:     Feature removal"
	@echo "  security:    Security fix"
	@echo "  performance: Performance improvement"
	@echo "  other:       Other"
