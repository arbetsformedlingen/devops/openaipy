# NAME

openaipy - a nice tool

# SYNOPSIS

**openaipy**
\[_option_...\] _action_

# DESCRIPTION

A tool.


# ACTIONS

Blurb

- **--nice** _hello_...

    Interesting text.

- **--help**

    Show help text.

# OPTIONS

- **--dir** -_directory_

    Use _directory_

# EXIT STATUS

- **0**

    The requested action was successfully performed.
    Or a check or assertion command returned true.

- **1**

    A check or assertion command returned false.

# ENVIRONMENT

- **PWD**

    The interesting path

# FILES

- _%hi%/.ho.\*_

    More text

# EXAMPLES

Run this for help

>     openaipy --help

# AUTHORS

{{AUTHOR}}

# COPYRIGHT

This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.
